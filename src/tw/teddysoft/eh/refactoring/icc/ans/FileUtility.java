/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.refactoring.icc.ans;

import java.io.IOException;
import org.junit.Test;

public class FileUtility {

	public void moveFiles(String srcFolder, String destFolder)  throws IOException {
		FolderCheckpoint cp = new FolderCheckpoint();
		
		try  {
			cp.establish(srcFolder);
			// 複製 srcFolder 所有檔案到 destFolder,
			// 可能會發生 IOException
		} catch(Exception e){
			cp.restore();
			throw e;
		} finally{
			cp.drop();
		}
	}
}
