/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.sparehandler.exercise;

import static org.junit.Assert.*;
import org.junit.Test;

public class ACLManagerTest {

	@Test
	public void introduce_resourceful_try_block() {
		ACLManager acl = new ACLManager();
		
		try {
			acl.readUser("Teddy");
			fail();
		} catch (ReadUserException e) {
			assertTrue(true);
		
		}
	}

}
