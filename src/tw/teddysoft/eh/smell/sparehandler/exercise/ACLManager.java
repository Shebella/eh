/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.sparehandler.exercise;

import java.io.IOException;
import java.sql.SQLException;

public class ACLManager {

	public User readUser(String name) throws ReadUserException {
		try {
			return readFromDatabase(name);  // 可能丟出SQLException
		} catch(Exception e){
			try {
				return readFromLDAP(name);  // 可能丟出IOException
			} catch(IOException ex){
				throw new ReadUserException(ex);
			}
		}
	}

	
	private User readFromDatabase(String name) throws SQLException{
		if (true) throw new SQLException();
		return null;
	}

	private User readFromLDAP(String name) throws IOException {
		if (true) throw new IOException();
		return null;
	}
	

}
