/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.sparehandler.exercise;


public class ReadUserException extends Exception {

	public ReadUserException(Throwable e) {
		super(e);
	}

}
