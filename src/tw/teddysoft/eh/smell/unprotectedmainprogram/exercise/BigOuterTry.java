/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.unprotectedmainprogram.exercise;

public class BigOuterTry {

	static public void main(String [] args){
		
		//TODO Avoid unexpected termination with big outer try block
		
		/*
		 * 做一大堆事情的主程式  
		 */
	}
}
