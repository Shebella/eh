/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.dummyhandler.exercice;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileUtility {
	
	public static void  writeFile(String fileName, String data) {
		try (Writer writer = new FileWriter(fileName)) {   
		    writer.write(data);  /* may throw an IOException */  
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
}
