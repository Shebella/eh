/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.dummyhandler.ans;

import static org.junit.Assert.*;
import org.junit.Test;

public class FileUtilityTest {

	@Test
	public void replace_dummy_handler_with_rethrow() {
		try{
			FileUtility.writeFile(".", "This is a test.");
			fail();
		}
		catch(UnhandledException e){
			assertTrue(true);
		}
	}

}
