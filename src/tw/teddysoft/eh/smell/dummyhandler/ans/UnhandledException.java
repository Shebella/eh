/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.dummyhandler.ans;

public class UnhandledException extends RuntimeException {

	public UnhandledException(){
		super();
	}

	public UnhandledException(Throwable e){
		super(e);
	}
	
	public UnhandledException(String msg){
		super(msg);
	}

	public UnhandledException(String msg, Throwable e){
		super(msg, e);
	}
	
}
