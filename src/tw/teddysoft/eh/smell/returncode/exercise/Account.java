/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.returncode.exercise;

public class Account {
	private  int balance;
	
	public Account(int balance){
		this.balance = balance;
	}
	
	public synchronized int withdraw(int amount) {
	      if (amount > this.balance)
	             return -1;
	      else {
	             this.balance = this.balance - amount;     
	              return this.balance;
	    }
	} 
	
}
