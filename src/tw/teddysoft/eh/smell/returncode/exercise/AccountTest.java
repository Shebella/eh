/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.returncode.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class AccountTest {

	@Test
	public void when_withdraw_200_from_1000_then_balance_800() throws NotEnoughMoneyException {
		Account account = new Account(1000);
		assertEquals(800, account.withdraw(200));	
	}
	
	@Test
	public void replace_error_code_with_exception() {
		Account account = new Account(1000);
		try{
			account.withdraw(5000);
			fail();
		} 
		catch(NotEnoughMoneyException e){
			assertTrue(true);
		}
	}
}
