/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.returncode.ans.step2;

public class Account {
	private int balance;
	
	public Account(int balance){
		this.balance = balance;
	}
	
	public synchronized int withdraw(int amount) throws NotEnoughMoneyException {
	      if (amount > this.balance)
	             throw new NotEnoughMoneyException();
	      
	       this.balance = this.balance - amount;     
	       return this.balance;
	} 
}
