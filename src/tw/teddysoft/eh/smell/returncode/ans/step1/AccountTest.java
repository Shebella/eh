/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.returncode.ans.step1;

import static org.junit.Assert.*;

import org.junit.Test;

public class AccountTest {
	
	@Test
	public void when_withdraw_200_from_1000_then_balance_800() {
		Account account = new Account(1000);
		assertEquals(800, account.withdraw(200));	
	}
	
	@Test
	public void when_withdraw_mony_more_than_balance_then_the_result_is_nagative_1() {
		Account account = new Account(1000);
		assertEquals(-1, account.withdraw(5000));	
	}
}
