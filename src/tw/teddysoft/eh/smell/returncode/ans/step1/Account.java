/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.returncode.ans.step1;

public class Account {
	private int balance;
	
	public Account(int balance){
		this.balance = balance;
	}
	
	public synchronized int withdraw(int amount) {
		try{
			return newWithdraw(amount);
		}
		catch(NotEnoughMoneyException e){
			return -1;
		}
	}
		
	public synchronized int newWithdraw(int amount) throws NotEnoughMoneyException {
	      if (amount > this.balance)
	             throw new NotEnoughMoneyException();
	      
	       this.balance = this.balance - amount;     
	       return this.balance;
	} 

	
}
