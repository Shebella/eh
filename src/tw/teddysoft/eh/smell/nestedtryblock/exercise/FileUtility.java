/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.nestedtryblock.exercise;

import java.io.FileInputStream;
import java.io.IOException;

public class FileUtility {

	public static void readConfig(String fileName){
		FileInputStream in = null;
	    try { 
	    	in = new FileInputStream(fileName);
	    	      // normal logic
	    } catch (IOException e){
	          // exception handler
	    } 
	    finally {
	    		//TODO 	Replace Nested Try Block with Method
		    	try {
		    		if (in != null) in.close();
		    	} catch(IOException e) { 
		        	// log the exception 
		    }
	    }
	} 
}
