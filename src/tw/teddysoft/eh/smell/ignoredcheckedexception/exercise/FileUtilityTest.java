/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.ignoredcheckedexception.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class FileUtilityTest {

	@Test
	public void replace_ignored_checked_exception_with_unchecked_exception() {
		try{
			FileUtility.writeFile(".", "This is a test.");
			fail();
		}
		catch(UnhandledException e){
			assertTrue(true);
		}
	}

}
