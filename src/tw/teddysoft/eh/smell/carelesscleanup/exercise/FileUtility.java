/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.carelesscleanup.exercise;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileUtility {
	
	public static void readData(String fileName) {
		try {
				FileInputStream fis = new FileInputStream(new File(fileName));
				
				// reading data
				
				fis.close();
		} 
		catch (IOException e) {  
			throw new RuntimeException(e);
		} 
	}
}
